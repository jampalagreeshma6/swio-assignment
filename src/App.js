
import './App.css';

import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home from './Components/Home';
import Payment from './Components/Payment';
import New from './Components/New';


function App() {
  return (
    <div>
   <BrowserRouter>
    <Routes>
      <Route path="/" element={<Home/>}/>
       <Route path="/Payment" element={<Payment/>} />
       <Route path="/New" element={<New/>} />
    </Routes>
  </BrowserRouter>
 

  </div>
  );
}
export default App;
